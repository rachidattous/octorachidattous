package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.MontantMaxException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.service.AuditService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class TransferRepositoryTest {

  @Autowired
  private TransferRepository transferRepository;

  @Autowired
  private CompteRepository compteRepository;

  @Autowired
  private UtilisateurRepository utilisateurRepository;

  @Autowired
  private AuditService auditService;


  @Test
  public void saveComptes() throws Exception{
    Utilisateur utilisateur1 = new Utilisateur();
    utilisateur1.setUsername("user1");
    utilisateur1.setLastname("last1");
    utilisateur1.setFirstname("first1");
    utilisateur1.setGender("Male");

    utilisateurRepository.save(utilisateur1);


    Utilisateur utilisateur2 = new Utilisateur();
    utilisateur2.setUsername("user2");
    utilisateur2.setLastname("last2");
    utilisateur2.setFirstname("first2");
    utilisateur2.setGender("Female");

    utilisateurRepository.save(utilisateur2);

    Compte compte1 = new Compte();
    compte1.setNrCompte("010000A000001000");
    compte1.setRib("RIB1");
    compte1.setSolde(200000L);
    compte1.setUtilisateur(utilisateur1);

    compteRepository.save(compte1);

    Compte compte2 = new Compte();
    compte2.setNrCompte("010000B025001000");
    compte2.setRib("RIB2");
    compte2.setSolde(140000L);
    compte2.setUtilisateur(utilisateur2);

    compteRepository.save(compte2);

    Transfer v = new Transfer();
    v.setMontantTransfer(10);
    v.setCompteBeneficiaire(compte2);
    v.setCompteEmetteur(compte1);
    v.setDateExecution(new Date());
    v.setMotifTransfer("Assignment 2021");

    transferRepository.save(v);
  }

  @Test
  public void findAllComptes() throws Exception{
    List<Compte> compteList = compteRepository.findAll();
    compteList.stream().forEach(System.out::println);
  }

  @Test
  public void findAllTransfers() throws Exception{
    System.out.println("Transfer List");
    List<Transfer> transferList = transferRepository.findAll();
    transferList.stream().forEach(System.out::println);
  }

  @Test
  public void saveTrasfer() throws Exception {
    List<Compte> compteList = compteRepository.findAll();
    compteList.stream().forEach(System.out::println);
    auditService.transfer(compteList.get(0).getNrCompte(),compteList.get(1).getRib(),20);
    auditService.transfer(compteList.get(0).getNrCompte(),compteList.get(1).getRib(),11000);
    auditService.transfer(compteList.get(0).getNrCompte(),compteList.get(1).getRib(),20);
    List<Transfer> transferList = transferRepository.findAll();
    transferList.stream().forEach(System.out::println);
  }

  @Test
  public void delete() throws Exception{
    List<Transfer> transferList = transferRepository.findAll();
    transferRepository.deleteById(transferList.get(0).getId());
    transferList.stream().forEach(System.out::println);
  }
}