package ma.octo.assignement.domain.util;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
public enum EventType {

  TRANSFER("transfer"),
  DEPOSIT("Deposit d'argent");

  private String type;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}
