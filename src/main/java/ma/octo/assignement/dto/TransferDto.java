package ma.octo.assignement.dto;

import lombok.Data;

import java.util.Date;

@Data
public class TransferDto {
  private String nrCompteEmetteur;
  private String nrCompteBeneficiaire;
  private String motif;
  private double montant;
  private Date date;
}
