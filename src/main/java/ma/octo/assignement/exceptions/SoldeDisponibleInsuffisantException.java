package ma.octo.assignement.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class SoldeDisponibleInsuffisantException extends Exception {

  private static final long serialVersionUID = 1L;

  public SoldeDisponibleInsuffisantException(String message) {
    super(message);
  }
}
