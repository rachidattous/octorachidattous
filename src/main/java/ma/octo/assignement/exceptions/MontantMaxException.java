package ma.octo.assignement.exceptions;

public class MontantMaxException extends Exception{
    private static final long serialVersionUID = 1L;

    public MontantMaxException(String message) {
        super(message);
    }
}
