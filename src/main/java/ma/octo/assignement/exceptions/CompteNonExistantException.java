package ma.octo.assignement.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CompteNonExistantException extends Exception {

  private static final long serialVersionUID = 1L;

  public CompteNonExistantException(String message) {
    super(message);
  }
}
