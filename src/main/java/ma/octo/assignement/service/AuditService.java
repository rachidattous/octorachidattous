package ma.octo.assignement.service;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.AuditTransfer;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.MontantMaxException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.repository.AuditTransferRepository;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class AuditService {
    Logger LOGGER = LoggerFactory.getLogger(AuditService.class);

    @Autowired
    private AuditTransferRepository auditTransferRepository;

    @Autowired
    private TransferRepository transferRepository;

    @Autowired
    private CompteRepository compteRepository;

    public void auditTransfer(String message) {

        LOGGER.info("Audit de l'événement {}", EventType.TRANSFER);

        AuditTransfer audit = new AuditTransfer();
        audit.setEventType(EventType.TRANSFER);
        audit.setMessage(message);
        auditTransferRepository.save(audit);
    }


    public void auditDeposit(String message) {

        LOGGER.info("Audit de l'événement {}", EventType.DEPOSIT);

        AuditTransfer audit = new AuditTransfer();
        audit.setEventType(EventType.DEPOSIT);
        audit.setMessage(message);
        auditTransferRepository.save(audit);
    }

    public void deposit(String accountId, double amount, String motif, String nom_prenom_emetteur) throws CompteNonExistantException {
        Compte compte = compteRepository.findByNrCompte(accountId);
        if(compte == null)
            throw new CompteNonExistantException("Bank Account Not Found");
        MoneyDeposit moneyDeposit = new MoneyDeposit();
        moneyDeposit.setMontant(amount);
        moneyDeposit.setDateExecution(new Date());
        moneyDeposit.setMotifDeposit(motif);
        moneyDeposit.setCompteBeneficiaire(compte);
        moneyDeposit.setNom_prenom_emetteur(nom_prenom_emetteur);
        AuditTransfer auditTransfer = new AuditTransfer();
        auditTransfer.setMessage(motif);
        auditTransfer.setEventType(EventType.DEPOSIT);
        auditTransferRepository.save(auditTransfer);
        compte.setSolde(compte.getSolde()+amount);
        compteRepository.save(compte);
        auditDeposit(motif);
    }

    public void transfer(String accountIdSource, String rib, double amount) throws CompteNonExistantException, SoldeDisponibleInsuffisantException, MontantMaxException {
        Compte compteSource = compteRepository.findByNrCompte(accountIdSource);
        if(compteSource == null)
            throw new CompteNonExistantException("Source Bank Account Not Found");
        List<Compte> compteList = compteRepository.findAll();
        Compte compteDestination = (Compte) compteList.stream()
                .filter(compte -> compte.getRib().equals(rib));

        if(compteDestination == null)
            throw new CompteNonExistantException("Destination Bank Account Not Found");
        if(amount > 10000)
            throw new MontantMaxException("Montant Maximum 10000DH");
        if(compteSource.getSolde() < amount)
            throw new SoldeDisponibleInsuffisantException("Solde Disponible Insuffisant");
        compteSource.setSolde(compteSource.getSolde()-amount);
        compteRepository.save(compteSource);
        deposit(compteDestination.getNrCompte(), amount, "Transfer From "+accountIdSource,compteDestination.getUtilisateur().getUsername());
        Transfer transfer = new Transfer();
        transfer.setMontantTransfer(amount);
        transfer.setCompteEmetteur(compteSource);
        transfer.setCompteBeneficiaire(compteDestination);
        transfer.setDateExecution(new Date());
        transfer.setMotifTransfer(motif(transfer));
        transferRepository.save(transfer);
        AuditTransfer auditTransfer = new AuditTransfer();
        auditTransfer.setMessage(motif(transfer));
        auditTransfer.setEventType(EventType.DEPOSIT);
        auditTransfer(motif(transfer));
    }

    public String motif(Transfer transfer){
        return "Transfer depuis " + transfer.getCompteEmetteur().getNrCompte() + " vers " + transfer
                .getCompteBeneficiaire().getNrCompte() + " d'un montant de " + transfer.getMontantTransfer();
    }
}
